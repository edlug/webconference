#!/usr/bin/env bash

set -euo pipefail

# The following is a config text area
# Only the last entry of a same name is taken into account
# Single-line only
# Letters, numbers and dashes only in key name
# Follow key name with "===" then immediately with data

: <<'EOCONFIG'

dbname===open503
dbuser===omuser
dbpass===lw38i4yueit/wilh378

EOCONFIG

# Config extraction script

([[ "$1" =~ ^[a-zA-Z0-9-]+$ ]] &&
    grep -Po "(?<=$1===).+" "$0"|tail -n1) ||
    echo "Invalid query '$1'"
