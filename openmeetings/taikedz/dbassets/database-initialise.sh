#!/usr/bin/env bash

here="$(dirname "$0")"

getconf() {
    "$here/config.sh" "$1"
}

db_name="$(getconf dbname)"

mysql -u root <<EODBSETUP

CREATE DATABASE "${db_name}" DEFAULT CHARACTERSET 'utf8';

GRANT ALL PRIVILEGES ON "${db_name}" TO '$(getconf dbuser)'@'*' IDENTIFIED BY '$(getconf dbpass)' WITH GRANT OPTION;

EODBSETUP
