#!/usr/bin/env bash

# Runs and logs a service

servicename="$1"; shift
logdir="/var/log/$servicename"

if [[ ! -d "$logdir" ]]; then
    mkdir "$logdir"
fi

while true; do
    "$@" 1>"$logdir/$servicename-out.log" 2>"$logdir/$servicename-err.log"

    sleep 1 # prevent an instantly-failing service from chewing up the CPU via the loop
done
