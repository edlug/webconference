#!/us/bin/env bash

set -euo pipefail

# Based on install PDF

askpass() {
    read -s -p "$* : " dbpass
    echo >&2

    [[ -n "$dbpass" ]] || { echo "A password must be provided!" >&2; exit 1 ; }

    echo "$dbpass"
}

OPMV="5.0.0-M3"
OPMD="apache-openmeetings-$OPMV"
OPMF="$OPMD.tar.gz"
OPM_URL="http://archive.apache.org/dist/openmeetings/$OPMV/bin/$OPMF"

DBCONN_V="8.0.18"
DBCONN_F="mysql-connector-java-$DBCONN_V.jar"
DBCONN_URL="https://repo1.maven.org/maven2/mysql/mysql-connector-java/$DBCONN_V/$DBCONN_F"

MDPWD="$(askpass "MySQL root password")"
HOLAPWD="$(askpass "Database password")"

read -p "--- remember these passwords !!! ---" -t 3 ||
    echo # force newline after message if simply timed out

cont() {
    read -p "Press enter to continue ..."
}

db-run() {
    sudo mysql -u root -p"$MDPWD" "$@"
}

enablestart() {
    local service
    for service in "$@"; do
        sudo systemctl enable "$service"
        sudo systemctl start "$service"
    done
}

# ===
CRED="\033[31m"
CGRN="\033[32m"
CYEL="\033[33m"
CBLU="\033[34m"
CPUR="\033[35m"
CDEF="\033[0m"

faile() {
    echo -e "${CRED}$*${CDEF}" >&2
    exit 1
}

infoe() {
    echo -e "${CBLU}$*${CDEF}" >&2
}

progresse() {
    echo -e "${CPUR}$*${CDEF}" >&2
}
# ---

packages=(
    openjdk-11-jdk
    openjdk-11-jdk-headless

    libreoffice

    imagemagick
    libjpeg62
    zlib1g-dev

    ffmpeg
    sox

    ghostscript

    mariadb-server

    kurento-media-server
)

progresse "Adding keys and repositories"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 5AFA7A83 # Kurento
echo "
deb [arch=amd64] http://ubuntu.openvidu.io/6.10.0 bionic kms6
deb [arch=amd64] http://mirror.yandex.ru/ubuntu/ bionic main restricted
deb [arch=amd64] http://mirror.yandex.ru/ubuntu/ bionic universe
" | sudo tee /etc/apt/sources.list.d/kurento-dev.list >/dev/null

sudo add-apt-repository ppa:libreoffice/ppa
sudo apt-get update

progresse Installing "${packages[*]}"

sudo apt-get install -y "${packages[@]}"

progresse Update alternatives

sudo update-alternatives --config java

progresse Update imagemagick policy

sudo sed -r -e 's#(<policy domain="coder".+pattern="(PS|PDF)".*/>)#<!-- \1 -->#' -i /etc/ImageMagick-6/policy.xml

progresse Start services

enablestart mariadb
sudo /etc/init.d/kurento-media-server start

# ==========================
progresse Configure database

sudo mysqladmin -u root password "$MDPWD"

(echo "CREATE DATABASE open503 DEFAULT CHARACTER SET 'utf8';
GRANT ALL PRIVILEGES ON open503.* TO 'hola'@'localhost' IDENTIFIED BY '$HOLAPWD' WITH GRANT OPTION;
" | db-run) || faile "DB reconfiguration failed"

# ==========================
progresse Configure Kurento

sudo sed -r -e 's/^DAEMON_USER="kurento"/DAEMON_USER="nobody"/' -i /etc/default/kurento-media-server
grep -n 'DAEMON_USER="nobody"' /etc/default/kurento-media-server || faile "Kurento config failed"

sudo /etc/init.d/kurento-media-server restart

# ==========================
# OpenMeetings install proper

cd /opt

progresse Getting OPM

sudo wget "$OPM_URL"
sudo tar xzf "$OPMF"
sudo mv "${OPMD}" open503

progresse Adjusting OPM directories

sudo mkdir -p open503/webapps/openmeetings/data/streams/{1..14}
sudo mkdir -p open503/webapps/openmeetings/data/streams/hibernate
sudo chmod -R 750 open503/webapps/openmeetings/data/streams/
sudo chown -R nobody open503/

progresse Getting connector

sudo wget "$DBCONN_URL"
sudo cp "$DBCONN_F" open503/webapps/openmeetings/WEB-INF/lib/

sudo wget "https://cwiki.apache.org/confluence/download/attachments/27838216/tomcat3"
sudo cp tomcat3 /etc/init.d/
sudo chmod 750 /etc/init.d/tomcat3

sudo /etc/init.d/tomcat3 start

infoe "Package installation performed."
infoe "Wait 40 sec for startup, then visit on HTTPS, port 5443"

cat <<SETUPHELP

Press the '>' button on the welcome screen, to proceed to DB linkup

DB type: MySQL
DB host: localhost
DB prot: 3306
DB name: open503
DB user: hola
DB pass: $HOLAPWD

User data screen:

Username = your admin username
Userpass = your admin password
Email    = your email address
User time zone = time zone for the user
Group(Domains) Name = name of the group for your user to be in

Configuration screen:

Mail-Referer: what address mails to users will be From

Leave SMTP server empty unless you have a SMTP server

Cpmverters screen:

Leave DPI and Quality to default

For ImageMagick, FFMPEG and SoX paths, use '/usr/bin'

For OpenOffice/LibreOffice path, use /usr/lib/libreoffice

Crypt Type and red5SIP screen:

Leave defaults

Click Finish

DO NOT "Enter the Application yet !!!

Run 'sudo /etc/init.d/tomcat3 restart'

And then Enter the Application

=====

Server maintenance commands:

systemctl [stop|start|restart] mariadb
sudo /etc/init.d/kurento-media-server [stop|start|restart]
sudo /etc/init.d/tomcat3 [stop|start|restart]

SETUPHELP
