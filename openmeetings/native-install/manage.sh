#!/usr/bin/env bash

s_tomcat3() { sudo /etc/init.d/tomcat3 "$@"; }
s_kurento() { sudo /etc/init.d/kurento-media-server "$@"; }

start_services() {
    sudo systemctl start mariadb # Ensure this is started
    s_kurento start
    s_tomcat3 start
}

stop_services() {
    s_tomcat3 stop
    s_kurento stop
    # Do not stop the database
}

case "$1" in
start)
    start_services
    ;;
stop)
    stop_services
    ;;
restart)
    stop_services
    start_services
    ;;
esac
