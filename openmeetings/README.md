# Apache Open Meetings

Download from [download page][aom_dlpage]

See [install page][aom_instalpage]

Install [manual pdf][aom_installman]

## Native install

A script is provided for performing native installations of the Apache OpenMeetings

```sh
bash native-install/install.sh
```

You will be prompted for the root password for the MariaDB server, as well as a user password for the user.

After this, you will be prompted for the sudo password, and when the script pauses when adding package signature keys, press Enter to proceed.

The script will then run the installation unattended, and finally print out the last few hints for the web-based installation.


TODO: document where the data is stored, for backup/restore. In principle, it should be all database, though some files will probably also be involved.



[aom_dlpage]: https://openmeetings.apache.org/downloads.html
[aom_installpage]: https://openmeetings.apache.org/installation.html
[aom_installman]: https://cwiki.apache.org/confluence/download/attachments/27838216/Installation%20OpenMeetings%205.0.0-M3%20on%20Ubuntu%2018.04%20LTS.pdf?version=6&modificationDate=1579175352000&api=v2
