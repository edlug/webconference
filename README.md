# Webconferencing server for EdLUG

A test with creating a web-conferencing server for EdLUG during the COVID-19 outbreak

## Purpose

Host an online meeting solution that is open-source, fully compatible with Chromium and Firefox, and hopefully not requiring a bespoke software/plugin download.

## Design goals

Ideally we want something that can be deployed to a fresh server, and easily backed up, and restored.

In Tai's ideal world for now, this would be a Docker installation, though direct-installation is valid as well, so long as the backup/restore mechanisms are well understood.

## Proposed

### Apache OpenMeetings

AOM is listed on [an elearning site][elearn]

### GNU Jami / Ring.cx

Ring was renamed to [GNU Jami][jami]


[jami]: https://jami.net
[elearn]: https://elearningindustry.com/top-6-open-source-web-conferencing-software-tools-elearning-professionals>
