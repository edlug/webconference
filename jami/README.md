# GNU Jami

Jami requires no server, it is fully peer-to-peer. For that reason, trying it out might require a few different users...

Try installing GNU Jami using [a guide][jamiguide1]

[jamiguide1]: https://www.ubuntubuzz.com/2019/12/install-gnu-jami-on-ubuntu-and-trisquel.html
