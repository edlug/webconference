#!/usr/bin/env bash

set -euo pipefail

main() {
    . /etc/os-release
    PKGNAME=jami

    if [[ -n "$VERSION_ID" ]]; then
        VMAJOR="${VERSION_ID%.*}"
        VMINOR="${VERSION_ID#*.}"

        if [[ "$VMAJOR" = 16 ]]; then
            # Package name is jami in the back-compat repo
            setup_1604
        elif [[ "$VMAJOR" = 18 ]]; then
            # In the 18.04 repo, it is called 'ring', its old name
            PKGNAME=ring
        fi
    fi

    sudo apt-get update && sudo apt-get install "$PKGNAME"
}

setup_1604() {
    sudo apt install gnupg dirmngr ca-certificates curl --no-install-recommends
    curl -s https://dl.jami.net/public-key.gpg | sudo tee /usr/share/keyrings/jami-archive-keyring.gpg > /dev/null
    sudo sh -c "echo 'deb [signed-by=/usr/share/keyrings/jami-archive-keyring.gpg] https://dl.jami.net/nightly/ubuntu_16.04/ ring main' > /etc/apt/sources.list.d/jami.list"
}

main "$@"
